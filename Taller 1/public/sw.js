importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');
if (workbox) {
    workbox.precaching.precacheAndRoute([
  {
    "url": "images/192.jpg",
    "revision": "8b61a265af4e15012c1e692972a100bc"
  },
  {
    "url": "images/512.jpg",
    "revision": "70a5ea65c442beb7864dcd039ba11e03"
  },
  {
    "url": "images/favicon.ico",
    "revision": "5727d91491e70d3c33a702327d8219ea"
  },
  {
    "url": "images/ic_add_white_24px.svg",
    "revision": "b09442e8f4b45894cf21566f0813453c"
  },
  {
    "url": "images/ic_refresh_white_24px.svg",
    "revision": "21e4c77a8b98c7516d6c7a97cdbddc22"
  },
  {
    "url": "index.html",
    "revision": "ccc2ac8bf1de721463ecdf264b1d4be0"
  },
  {
    "url": "manifest.json",
    "revision": "c0d8911d017ea88edc7e75762883ff11"
  },
  {
    "url": "scripts/app.js",
    "revision": "6a70069fb69907c6fe69f0238f3cb907"
  },
  {
    "url": "service-worker.js",
    "revision": "92490904a6dae44cc241be46fb8b3c8e"
  },
  {
    "url": "styles/inline.css",
    "revision": "964e8546d971e6d204fd2644c3ed4abf"
  }
]);
    workbox.routing.registerRoute(/api-ratp.pierre-grimaud/, 
        new workbox.strategies.NetworkFirst({cacheName: 'api-cache',})
    );
}

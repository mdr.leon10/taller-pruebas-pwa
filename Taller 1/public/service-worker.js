importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');
if (workbox) {
    workbox.precaching.precacheAndRoute([]);
    workbox.routing.registerRoute(/api-ratp.pierre-grimaud/, 
        new workbox.strategies.NetworkFirst({cacheName: 'api-cache',})
    );
}

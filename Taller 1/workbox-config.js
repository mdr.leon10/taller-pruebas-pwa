module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{jpg,ico,svg,html,json,js,css}"
  ],
  "swDest": "public\\sw.js",
  "swSrc": "public/service-worker.js"
};
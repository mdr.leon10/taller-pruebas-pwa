describe('Los estudiantes login', function(){
	it('Visits los estudiantes, tries to sign up but failes', function(){
		//Goes to the site
		cy.visit('https://losestudiantes.co')
		//Closes the pop up
		cy.contains('Cerrar').click()
		//Clicks the sign in/ sign up button
		cy.contains('Ingresar').click()
		// Starts filling the form with the name
		cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Maria")
		//Fills the form with the lastname
		cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Leon")
		//Fills the form with the mail address
		cy.get('.cajaSignUp').find('input[name="correo"]').click().type("mdr.leon10@uniandes.edu.co")
		//Selects a university from the list of universities
		cy.get('[name=idUniversidad]').select('pontificia-universidad-javeriana')
		//Selects the department
		cy.get('[name=idDepartamento]').select('451')
		//Fills the form with the password
		cy.get('.cajaSignUp').find('input[name="password"]').click().type("12345678")
		//Accepts terms and conditions
		cy.get('.cajaSignUp').find('input[name="acepta"]').click()
		//Submits the form
		cy.get('.cajaSignUp').contains('Registrarse').click()
		//Validates that the sign up failed
		cy.contains('Ocurrió un error activando tu cuenta')
		//TODO Boton OK
		cy.get('.sweet-alert').contains('Ok').click()
	})
})

describe('Los estudiantes login', function() {
    it('In losestudiantes, login with valid account, search for a professor, filter comments by subject', function() {
      	// Fills the mail box
      	cy.get('.cajaLogIn').find('input[name="correo"]').click().type("ejemplo@gmail.com")
      	//Fills the password box
      	cy.get('.cajaLogIn').find('input[name="password"]').click().type("Contraseña1234567")
      	//Clicks the enter button
      	cy.get('.cajaLogIn').contains('Ingresar').click()
      	//Searches for professor Mario Linarez
      	cy.get('.buscador').find('input[role="combobox"]').type("Mario Linares", { force: true })
      	//Selects the professor
      	cy.contains("Mario Linares Vasquez - Ingeniería de Sistemas").click()
      	//Filters the comments by subject
      	cy.get('.boxElement').find('input[name="id:ISIS3510"]').click()
    })
})	